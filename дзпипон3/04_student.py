# -*- coding: utf-8 -*-

# (цикл while)

# Ежемесячная стипендия студента составляет A руб., а расходы на проживание превышают стипендию
# и составляют В руб. в месяц. Рост цен ежемесячно увеличивает расходы на 3%.
# Составьте программу расчета суммы денег, которую необходимо единовременно попросить у родителей,
# чтобы можно было прожить учебный год (10 месяцев), используя только эти деньги и стипендию.

A, B = 10000, 12000

step_total = A * 10
summ = 0
month = 1
while month != 10:
    B = B * 1.03
    summ += B
    month += 1
    print(month)
print(summ - step_total)

# TODO - При расчетах не учли расходы в первом месяце. Студенту не хватит денег и он умрет с голоду
