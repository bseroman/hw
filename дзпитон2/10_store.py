#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Есть словарь кодов товаров

goods = {
    'Лампа': '12345',
    'Стол': '23456',
    'Диван': '34567',
    'Стул': '45678',
}

# Есть словарь списков количества товаров на складе.

store = {
    '12345': [
        {'quantity': 27, 'price': 42},
    ],
    '23456': [
        {'quantity': 22, 'price': 510},
        {'quantity': 32, 'price': 520},
    ],
    '34567': [
        {'quantity': 2, 'price': 1200},
        {'quantity': 1, 'price': 1150},
    ],
    '45678': [
        {'quantity': 50, 'price': 100},
        {'quantity': 12, 'price': 95},
        {'quantity': 43, 'price': 97},
    ],
}

# Рассчитать на какую сумму лежит каждого товара на складе
# например для ламп

# lamps_cost = store[goods['Лампа']][0]['quantity'] * store[goods['Лампа']][0]['price']
# # или проще (/сложнее ?)
# lamp_code = goods['Лампа']
# lamps_item = store[lamp_code][0]
# lamps_quantity = lamps_item['quantity']
# lamps_price = lamps_item['price']
# lamps_cost = lamps_quantity * lamps_price
# print('Лампа -', lamps_quantity, 'шт, стоимость', lamps_cost, 'руб')
# print('лампс айтем!!!',lamps_item)

# ывести стоимость каждого товара на складе
cost_all_kinds=0
# for name in (goods):
#         for i in store[goods[name]]:
#             cost = i['quantity'] * i['price']
#             cost_all_kinds = 0
#             print('''поштучно:''',cost)
#             cost_all_kinds+=cost
#         print('вид товара',cost_all_kinds)
#         print('---------------------------')

# Формат строки <товар> - <кол-во> шт, стоимость <общая стоимость> руб
flag=0
kek=0
for name in (goods):
    flag=0
    '''сколько видов одного товара'''
    cost_all_kinds = 0
    for i in store[goods[name]]:
        flag+=1
        cost = i['quantity'] * i['price']
        cost_all_kinds += cost
        # if flag == 1:
        #     print(name,'-','колво',i['quantity'],'стоимость',cost)
        # else:
        #     print('-','колво',i['quantity'],'стоимость',cost)
    print('общая стоимость вида товара', cost_all_kinds)
    print('---------------------------')
        #     cost_all_kinds+=cost
        # print('вид товара',cost_all_kinds)
        # print('---------------------------')

# TODO - Закомментированный код (не описание) необходимо удалить
# TODO - Давайте правильные имена переменным. Имя переменной должно отражать сущность её, что она хранит

# TODO - Приведите вывод в соответствии с заданием:
#  Формат строки <товар> - <кол-во> шт, стоимость <общая стоимость> руб
