#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Создайте списки:

# моя семья (минимум 3 элемента, есть еще дедушки и бабушки, если что)
my_family = ['Я', 'Мама', 'Папа', 'Сестра', 'Дед', 'Баба']
print(my_family)

# список списков приблизителного роста членов вашей семьи
my_family_height = [
    # ['имя', рост],
    ['Я', 178],
    ['mama', 160],
    ['papa', 175],
    ['sestra', 120],
    ['deda', 170],
    ['babushka', 156]
]

# Выведите на консоль рост отца
print(my_family_height[2][1])
# Выведите на консоль общий рост вашей семьи как сумму ростов всех членов
height = 0
# TODO - Это задание без циклов
for i in range(len(my_family_height)):
    height += my_family_height[i][1]
print(height)
