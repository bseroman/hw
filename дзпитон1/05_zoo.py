#!/usr/bin/env python
# -*- coding: utf-8 -*-

# есть список животных в зоопарке

zoo = ['lion', 'kangaroo', 'elephant', 'monkey', ]
# TODO - Посмотрите как нужно вставлять новый элемент в середину списока
zoo[2:] = zoo[1:]
zoo[1] = 'bear'
print(zoo)
# посадите медведя (bear) во 2-ю клетку
#  и выведите список на консоль

# добавьте птиц в последние клетки
birds = ['rooster', 'ostrich', 'lark', ]
#  и выведите список на консоль
zoo = zoo + birds
print(zoo)
# уберите слона
# TODO - Убрать нужно элемент не с помощью срезов, а по содержимому. Т.е. элемент 'elephant'
zoo[3:] = zoo[4:]
print(zoo)
#  и выведите список на консоль

# выведите на консоль в какой клетке сидит обезьяна (monkey)
print(zoo.index('monkey') + 1)
# и жаворонок (lark)
print(zoo.index('lark') + 1)
